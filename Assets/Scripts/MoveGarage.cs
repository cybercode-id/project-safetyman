﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGarage : MonoBehaviour
{
    public float speed = 0.2f; 
    public Vector3 pos;
    public Vector3 targetUp;
    public Vector3 targetDown;
    public GameObject objectTarget;

    public bool naik;
    public bool turun;
    // Start is called before the first frame update
    void Start()
    {
        pos = objectTarget.transform.localPosition;
    }

    // Update is called once per frame
    public void atas()
    {
        naik = true;
        turun = false;

    }

    public void bawah()
    {
        naik = false;
        turun = true;
    }
    void Update()
    {
        if (naik)
        { 
            if (pos.y < targetUp.y)
            {
                objectTarget.transform.Translate(new Vector3(0, speed, 0) * Time.deltaTime);
                pos = objectTarget.transform.localPosition;
            }
            else
            {
                turun = false;
            }
        }
        else if (turun)
        {

            if (pos.y > targetDown.y)
            {
                objectTarget.transform.Translate(new Vector3(0, -speed, 0) * Time.deltaTime);
                pos = objectTarget.transform.localPosition;
            }
            else
            {
                naik = false;
            }
        }
    }
}
