﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public Transform FirstPos;
    public Transform TargetPos;

    public float speed = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(FirstPos.position, TargetPos.position, Mathf.PingPong(Time.time / speed, 1));
    }

    private void move(Transform target)
    {
        transform.position = Vector3.Lerp(transform.position, target.position, speed);
    }
}
